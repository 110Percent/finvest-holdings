from getpass import getpass

from finvestholdings.auth import roles_and_capabilities


def init_text() -> None:
    print('Finvest Holdings')
    print('Client Holdings and Information System')


def initial_prompt_signup() -> bool:
    # Prompt user to login or register
    print('----------------------------------------')
    print("Type login to login or register to register.")
    while True:
        command = input(">>> ")
        if command == "login":
            return False
        elif command == "register":
            return True
        else:
            print("Invalid command. Please try again.")


def login_prompt(retry=False) -> (str, str):
    # Prompt user for username and password for login
    print('----------------------------------------')
    if retry:
        print("Invalid username or password. Please try again.")
    else:
        print("Please enter your username and password.")
    username = input("Username: ")
    password = getpass("Password: ")
    return username, password


def create_user_username_prompt(retry=False) -> str:
    # Prompt user for username for registration
    print('----------------------------------------')
    if retry:
        print("Please enter a different username.")
    else:
        print("Please enter a username.")
    username = input("Username: ")
    return username


def create_user_password_prompt(retry=False) -> str:
    # Prompt user for password for registration
    print('----------------------------------------')
    if retry:
        print("Please enter a different password.")
    else:
        print("Please enter a password.")
    password = getpass("Password: ")
    return password


def create_user_role_prompt(retry=False) -> str:
    # Prompt user for role for registration
    print('----------------------------------------')
    if retry:
        print("Please enter a valid role. Valid roles are: " + ", ".join(roles_and_capabilities.get_role_ids()) + ".")
    else:
        print("Please enter a role.")
    role = input("Role: ")
    return role


def print_welcome(username: str, role: str) -> None:
    # Show welcome message and role
    print('----------------------------------------')
    print(f"Welcome, {username}!")
    print(f'Your role is: {prettify_key(role)}.')


def print_capabilities(capabilities: dict) -> None:
    # Print capabilities
    print('----------------------------------------')
    print("You have the following capabilities:")
    for capability in capabilities:
        if isinstance(capabilities[capability], dict):
            print(prettify_key(capability) + ':')
            for sub_capability in capabilities[capability]:
                print('\t' + prettify_key(sub_capability) + ' ' + (
                    '(DISABLED)' if not capabilities[capability] else '(ENABLED)'))
        elif isinstance(capabilities[capability], bool):
            print(prettify_key(capability) + ' ' + ('(DISABLED)' if not capabilities[capability] else '(ENABLED)'))


def prettify_key(key: str) -> str:
    # Prettify a key by capitalizing the first letter of each word and replacing underscores with spaces
    return key.title().replace('_', ' ')

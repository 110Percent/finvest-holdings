import hashlib
import secrets


def gen_hash_and_salt(password: str) -> (str, str):
    """
    Generates a hash and salt for a password
    :param password: The password to hash
    :return: A tuple containing the hash and salt
    """

    # Generate a 256-bit (32-byte) salt
    salt = secrets.token_hex(32)

    # Hash the password with the salt
    hashed = pbkdf(password, salt)
    return hashed, salt


def pbkdf(password: str, salt: str) -> str:
    """
    Hashes a password with a salt using PBKDF2, returning the hash as a hex string
    Runs 500000 iterations of SHA256 on the salt and output of the previous iteration
    :param password: Password to hash
    :param salt: Salt to hash with
    :return: Hashed password as a hex string
    """
    return hashlib.pbkdf2_hmac('sha256', bytes(password, 'utf-8'), bytes(salt, 'utf-8'), 500000).hex()


def test_password(password: str, hash: str, salt: str) -> bool:
    """
    Tests a password against a hash and salt
    :param password: Password to test
    :param hash: Hash to test against
    :param salt: Salt to test against
    :return: True if the password matches the hash and salt, False otherwise
    """
    return hash == pbkdf(password, salt)

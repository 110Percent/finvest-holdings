from finvestholdings.auth import passwordfile, username_password_check, roles_and_capabilities
from finvestholdings.crypto import crypto
from finvestholdings.interface import interface


def main():
    # Initialize password file
    passwds = passwordfile.PasswordFile("passwd.txt")

    # Show initial interface text
    interface.init_text()

    # Prompt user to login or register
    should_signup = interface.initial_prompt_signup()

    # If registering, prompt user for username, password and role
    if should_signup:
        username = interface.create_user_username_prompt()

        # Check if username is valid and doesn't already exist
        while passwds.user_exists(username) or not username_password_check.username_valid(username):
            if passwds.user_exists(username):
                print("Username already exists.")
            elif not username_password_check.username_valid(username):
                print("Username must contain only letters, numbers, underscores, spaces and periods.")
            username = interface.create_user_username_prompt(retry=True)

        password = interface.create_user_password_prompt()
        # Check if password is valid
        while not username_password_check.password_valid(username, password):
            password = interface.create_user_password_prompt(retry=True)

        role = interface.create_user_role_prompt()
        # Check if role is valid
        while not roles_and_capabilities.role_exists(role):
            role = interface.create_user_role_prompt(retry=True)

        # Add user to password file
        passwds.add_user(username, password, role)
        print('Account created. Log in to continue.')

    username, password = interface.login_prompt()
    # Check if username and password match
    while not passwds.user_exists(username) or not crypto.test_password(password, *passwds.get_hash_and_salt(username)):
        username, password = interface.login_prompt(retry=True)

    # Show welcome message and capabilities
    interface.print_welcome(username, passwds.get_role(username))
    interface.print_capabilities(roles_and_capabilities.get_capabilities(passwds.get_role(username)))

import json

# Load roles from file
role_file = open('roles.json', 'r')
role_data = json.load(role_file)
role_file.close()


def get_visible_roles() -> list:
    """
    :return: A list of all visible roles and their capabilities
    """
    return [role for role in role_data['roles'] if 'hidden' not in role or not role['hidden']]


def get_role_ids() -> list:
    """
    :return: A list of role IDs for all visible roles
    """
    return [role['role'] for role in get_visible_roles()]


def role_exists(role_id: str) -> bool:
    """
    Checks if a role exists in the roles file
    :param role_id: The role name to check
    :return: True if the role exists, False otherwise
    """
    return role_id in get_role_ids()


def merge_dict(dict1: dict, dict2: dict) -> dict:
    """
    Performs a deep merge of two dictionaries and returns the result
    :param dict1: Dictionary 1
    :param dict2: Dictionary 2
    :return: Merged dictionary
    """
    merged = dict1.copy()
    for key in dict2:
        if isinstance(dict2[key], dict):
            if key not in dict1:
                merged[key] = dict2[key]
            else:
                merged[key] = merge_dict(dict1[key], dict2[key])
        else:
            merged[key] = dict2[key]
    return merged


def get_capabilities(role_id: str) -> dict:
    """
    Gets the capabilities for a role
    :param role_id: The role to get the capabilities for
    :return: Dictionary of capabilities
    """
    for role in role_data['roles']:
        if role['role'] == role_id:
            default_capabilities = get_capabilities(role['inherits']) if 'inherits' in role else {}
            return merge_dict(default_capabilities, role['capabilities'])
    raise Exception(f"Role {role_id} does not exist")


def has_capability(role_id: str, capability: str) -> bool:
    """
    Checks if a role has a capability. Capabilities can be nested using periods.
    For example, 'view_account_balance.other' would check if the role has the 'other' capability under
        the 'view_account_balance' capability.
    :param role_id: Role to check
    :param capability: Capability to check
    :return: True if the role has the capability, False otherwise
    """
    capabilities = get_capabilities(role_id)
    if '.' in capability:
        capability = capability.split('.')
        if capability[0] not in capabilities:
            return False
        if isinstance(capabilities[capability[0]], dict):
            return capability[1] in capabilities[capability[0]] and capabilities[capability[0]][capability[1]]
        else:
            return False
    else:
        return capability in capabilities and capabilities[capability]

import re

special_chars = ['!', '@', '#', '$', '%', '?', '*']

username_regex = re.compile(r'^[a-zA-Z0-9_ \'.]{3,20}$')

# Matches phone numbers such as (123) 456-7890, 123-456-7890, etc
phone_number_regex = re.compile(r'(?:\+\d{1,2}\s?)?(?:\(\d{1,4}\))?\s?\d{1,5}[\s.-]?\d{1,5}[\s.-]?\d{1,5}')

# Matches license plates such as ABCD123, ABCD 123, ABCD-123, etc
license_plate_regex = re.compile(r'[a-zA-Z]{4}\W?\d{3}')

# Matches dates such as 1 Jan, January 1, etc
date_regex = re.compile(
    r'\d{,2}\s?(?:jan(?:uary)?|feb(?:ruary)?|mar(?:ch)?|apr(?:il)?|may|june?|july?|aug(?:ust)?|sept(?:ember)?'
    r'|oct(?:ober)?|nov(?:ember)?|dec(?:ember)?)\s?\d{,2}', re.IGNORECASE)


def username_valid(username: str) -> bool:
    # Usernames can only contain alphanumeric characters, underscores, spaces and periods
    # (This is mainly to avoid colons, which are used as delimiters in the password file)
    return bool(username_regex.match(username))


def password_valid(username: str, password: str) -> bool:
    # Passwords must be at least 8 characters long
    if len(password) < 8:
        print("Password must be at least 8 characters long.")
        return False
    # Passwords must contain at least one uppsercase letter
    if password == password.lower():
        print("Password must contain at least one uppercase letter.")
        return False
    # Passwords must contain at least one lowercase letter
    if password == password.upper():
        print("Password must contain at least one lowercase letter.")
        return False
    # Password must contain at least one number
    if not any(char.isdigit() for char in password):
        print("Password must contain at least one number.")
        return False
    # Password must contain at least one special character
    if not any(char in special_chars for char in password):
        print("Password must contain at least one special character (!, @, #, $, %, ?, *).")
        return False
    # Passwords must not match a common password
    if matches_common_password(password):
        print("Password is too common.")
        return False
    # Passwords must not contain the username
    if username.lower() in password.lower():
        print("Password cannot contain username.")
        return False
    return True


def matches_common_password(password: str) -> bool:
    if matches_common_number_format(password):
        return True

    # Check if password is in the common password list
    lowercase = password.lower()
    with open('darkweb2017-top10000.txt', 'r') as file:
        for line in file:
            if lowercase == line.strip():
                return True
    return False


def matches_common_number_format(password: str) -> bool:
    # Check if password matches a common number format
    if phone_number_regex.match(password):
        print("Password cannot match a phone number.")
        return True
    if license_plate_regex.match(password):
        print("Password cannot match an Ontario license plate.")
        return True
    if date_regex.match(password):
        print("Password cannot match a date.")
        return True

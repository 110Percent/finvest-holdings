from finvestholdings.crypto import crypto


class PasswordFile:
    """
    A class for interacting with the password file, stored at passwd.txt
    """

    def __init__(self, file_path):
        self.file_path = file_path
        try:
            self.__file = open(file_path, 'r').close()
        except FileNotFoundError:
            self.__file = open(file_path, 'w+').close()
        except Exception as e:
            print(f"An error occurred: {e}")

    def user_exists(self, username) -> bool:
        """
        Checks if a user exists in the password file
        :param username: The username to check
        :return: True if the user exists, False otherwise
        """
        with open(self.file_path, 'r') as file:
            for line in file:
                if line.split(':')[0] == username:
                    return True
        return False

    def get_hash_and_salt(self, username) -> (str, str):
        """
        Gets the hash and salt for a user
        :param username: The username to get the hash and salt for
        :return: A tuple containing the hash and salt
        """
        with open(self.file_path, 'r') as file:
            for line in file:
                if line.split(':')[0] == username:
                    return line.split(':')[1].strip(), line.split(':')[2].strip()
        raise Exception(f"User {username} does not exist")

    def get_role(self, username) -> str:
        """
        Gets the role for a user
        :param username: The username to get the role for
        :return: The role for the user
        """
        with open(self.file_path, 'r') as file:
            for line in file:
                if line.split(':')[0] == username:
                    return line.split(':')[3].strip()
        raise Exception(f"User {username} does not exist")

    def add_user(self, username, password, role) -> None:
        """
        Adds a user to the password file. Generates a salt and hash for the password and writes the user to the file.
        :param username: The username to add
        :param password: The password to add
        :param role: The role to add
        :return: None
        """
        if self.user_exists(username):
            raise Exception(f"User {username} already exists")
        pass_hash, salt = crypto.gen_hash_and_salt(password)
        with open(self.file_path, 'a') as file:
            file.write(f"{username}:{pass_hash}:{salt}:{role}\n")

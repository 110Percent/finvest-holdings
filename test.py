import json

from finvestholdings.auth.roles_and_capabilities import has_capability
from finvestholdings.interface.interface import prettify_key

'''
This script runs tests on the access control mechanism.
It reads tests from tests.json and runs them against the access control mechanism to verify that
    each test user has the expected capabilities.
'''


def main():
    # Run tests
    with open("tests.json") as json_file:
        test_structure = json.load(json_file)
        for test in test_structure['tests']:
            test_result = run_test(test)
            if test_result:
                print("Test passed")
            else:
                print("Test failed")
                return
        print('----------------------------------------')
        print('All tests passed!')


def run_test(test_data: dict) -> bool:
    print('----------------------------------------')
    print(f"Running test: {test_data['username']}")
    print(f"Role: {prettify_key(test_data['role'])}")
    if 'test_capabilities' not in test_data:
        print('No capabilities to test')
        return False

    # Test capabilities
    for capability in test_data['test_capabilities']:
        # If capability is a dictionary, test each sub-capability
        if isinstance(test_data['test_capabilities'][capability], dict):
            for sub_capability in test_data['test_capabilities'][capability]:
                capability_key = capability + '.' + sub_capability
                # Get the expected value from the test
                expected = test_data['test_capabilities'][capability][sub_capability]
                print(f"Testing that capability {capability_key} is " + str(expected))
                # Get the actual value from the access control mechanism
                actual = has_capability(test_data['role'], capability_key)
                if not (expected == actual):
                    return False
        # Otherwise, test the capability itself
        else:
            # Get the expected value from the test
            expected = test_data['test_capabilities'][capability]
            print(f"Testing that capability {capability} is " + str(expected))
            # Get the actual value from the access control mechanism
            actual = has_capability(test_data['role'], capability)
            if not (expected == actual):
                return False

    return True


if __name__ == "__main__":
    main()
